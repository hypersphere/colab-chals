#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static inline __attribute__((always_inline)) void flag(void) {
    const char buf[] = {
        "cybears{" "5ta(k_Bu1l+_s7r" "}"
    };
    for (int i = 0; i < sizeof(buf); ++i) {
        putchar(buf[i] ^ 0xA5);
    }
}


int main(void) {
    /* This is just to add some noise to the program */
    unsigned long long n = 9780657630;  /* 1132 steps */

    while(n > 0){
        if (n <= 1) {
            flag();
            break;
        } else if (n % 2) {
            n = n * 3 + 1;
        } else {
            n = n / 2;
        }
        sleep(1);
    }

    return EXIT_SUCCESS;
}
