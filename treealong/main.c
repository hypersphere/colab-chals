#include <stdlib.h>

typedef struct node {
    struct node *left;
    const char val;
    struct node *right;
} node;

const node *root = &(node){
    .left = &(node){
        .left = &(node){
            .left = &(node){
                .val = 101,
                .right = &(node){
                    .left = &(node) { .val = 114 },
                    .val = 97,
                },
            },
            .val = 98,
            .right = &(node){
                .left = &(node) {
                    .left = &(node){ .val = 119 },
                    .val = 123,
                    .right = &(node){ .val = 51 },
                },
                .val = 115,
                .right = &(node){
                    .val = 108,
                    .right = &(node) { .val = 99 },
                },
            },
        },
        .val = 121,
        .right = &(node){
            .left = &(node){
                .left = &(node){ .val = 101 },
                .val = 109,
                .right = &(node){ .val = 95 },
            },
            .val = 48,
            .right = &(node){
                .left = &(node){
                    .left = &(node){ .val = 55 },
                    .val = 95,
                    .right = &(node){ .val = 104 },
                },
                .val = 50,
                .right = &(node){ .val = 51 },
            },
        },
    },
    .val = 99,
    .right = &(node){
        .left = &(node){
            .left = &(node){
                .left = &(node){
                    .val = 51,
                    .right = &(node){ .val = 51 },
                },
                .val = 114,
                .right = &(node){
                    .left = &(node){ .val = 48 },
                    .val = 104,
                    .right = &(node){ .val = 117 },
                },
            },
            .val = 43,
            .right = &(node){
                .left = &(node){ .val = 101 },
                .val = 53,
                .right = &(node){ .val = 125 },
            },
        },
        .val = 95,
        .right = &(node){ .val = 0 },
    },
};

static char g_char;

void preorder_walk(const node *p) {
    if (!p) return;
    g_char = p->val;
    preorder_walk(p->left);
    preorder_walk(p->right);
}

int main(void) {
    preorder_walk(root);
    return EXIT_SUCCESS;
}
